require 'celluloid/current'
require 'net/ping'

class PingWorker
  include Celluloid

  def perform
    ActiveRecord::Base.connection_pool.with_connection do
      Server.find_each(batch_size: 50) do |s|
        icmp = Net::Ping::External.new(s.name, nil, 60)
        p = Ping.new(server_name: s.name)
        if icmp.ping
          p.duration = icmp.duration
        else
          p.lost = true
        end    
        p.save
      end
    end 
  end
end
