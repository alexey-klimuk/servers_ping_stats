class CreateServers < ActiveRecord::Migration[5.0]
  def change
    create_table :servers do |t|
      t.string :name
      
      t.datetime :created_at 
    end
  end
end
