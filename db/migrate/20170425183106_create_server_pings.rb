class CreateServerPings < ActiveRecord::Migration[5.0]
  def change
    create_table :pings do |t|
      t.string :server_name
      t.float :duration
      t.boolean :lost, default: false

      t.datetime :created_at
    end
  end
end
