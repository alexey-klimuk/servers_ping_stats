class Ping < ActiveRecord::Base
  validates :server_name, presence: true
end
