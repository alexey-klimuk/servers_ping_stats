require 'sinatra/base'
require 'sinatra/activerecord'
require 'rufus/scheduler'
require 'json'

ActiveRecord::Base.schema_format = :sql

current_dir = Dir.pwd
Dir[
  "#{current_dir}/models/*.rb", 
  "#{current_dir}/workers/*.rb"
].each { |file| require file }

class ServersPingStats < Sinatra::Base
  configure do
    set :server, :puma
    set :scheduler, Rufus::Scheduler.new(blocking: false)
  end

  scheduler.every '10m' do
    ping_pool = PingWorker.pool
    ping_pool.async.perform
  end
  
  get '/servers' do
    content_type :json
    Server.pluck(:name).to_json
  end

  post '/servers' do
    content_type :json
    Server.create(name: params[:name])
  end

  delete '/servers/:name' do
    content_type :json
    Server.find_by(name: params[:name]).try(:destroy)
  end

  get '/servers/:name' do
    content_type :json
    return {} if params[:name].nil?
    scope = Ping.where(server_name: params[:name])
    if params[:start_date] && params[:end_date]
      scope = scope.where('created_at >= ? AND created_at <= ?', params[:start_date], params[:end_date])
    end
    s1 = scope.where(lost: false)
              .select('MAX(duration) AS max_ping, 
                       MIN(duration) AS min_ping, 
                       AVG(duration) AS avg_ping, 
                       MEDIAN(duration) AS med_ping, 
                       STDDEV(duration) AS stddev_ping')[0]
    s2 = scope.select('SUM(CASE WHEN lost=FALSE THEN 0 ELSE 1 END)::float/COUNT(lost) * 100 AS lost_percent')[0]
    { max: s1.max_ping, min: s1.min_ping, avg: s1.avg_ping, median: s1.med_ping, stddev: s1.stddev_ping, lost: "#{s2.lost_percent.to_f.round(2)} %" }.to_json
  end
end
